import argparse
import subprocess
import platform
import os

parser = argparse.ArgumentParser(
    description=(
        "Quick command to compile small C++ projects with basic flags set. This script "
        "assumes that LLVM is installed. On a Mac, it will prefer to use homebrew LLVM if it is "
        "found. It currently does not support Windows."
    )
)

defaultStandard = 23
parser.add_argument(
    "-cpp",
    type=int,
    choices=[11, 14, 17, 20, 23],
    default=defaultStandard,
    help=(
        f"Choose what C++ standard to use for compilation, C++{defaultStandard} is used if no "
        "standard is specified"
    ),
)
parser.add_argument(
    "-debug", action="store_true", help="Sets debug flag and lowers optimization level"
)
parser.add_argument("files", nargs="+", help="List all files that must be compiled")
args = parser.parse_args()

pathToClang = subprocess.check_output(["type", "-P", "clang++"], text=True).strip()
clangpp = pathToClang

myOS = platform.system()
if myOS == "Darwin":
    isBrewInstalled = (
        True
        if subprocess.run(
            ["type", "brew"], stdout=subprocess.PIPE, stderr=subprocess.PIPE
        ).stdout
        else False
    )
    if isBrewInstalled:
        brewClang = "/usr/local/opt/llvm/bin/clang++"
        if os.path.exists(brewClang):
            clangpp = brewClang

invocation = [clangpp, "-Wall", "-Wextra"]

# Handle debugging flags
if args.debug:
    invocation.append("-g")
    invocation.append("-Og")

# Handle C++ standard
std = "-std=c++"
if args.cpp == 11:
    std += "11"
elif args.cpp == 14:
    std += "14"
elif args.cpp == 17:
    std += "17"
elif args.cpp == 20:
    std += "20"
elif args.cpp == 23:
    std += "23"
else:
    std += "23"
invocation.append(std)

if myOS == "Darwin" and isBrewInstalled:
    invocation = invocation + [
        "-nostdinc++",
        "-nostdlib++",
        "-isystem",
        "/usr/local/opt/llvm/include/c++/v1",
        "-L/usr/local/opt/llvm/lib",
        "-Wl,-rpath,/usr/local/opt/llvm/lib",
        "-lc++",
        "-L\"$(brew --prefix llvm)\"/lib/c++",
    ]
else:
    invocation = invocation + ["-stdlib=libc++", "-fuse-ld=lld"]

# Add files; should always be last
for file in args.files:
    invocation.append(file)

subprocess.run(invocation)
