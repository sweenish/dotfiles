#!/bin/bash

sudo apt update && sudo apt install -y \
    build-essential     \
    clang		\
    clang-format	\
    cmake		\
    git		        \
    libc++-dev	        \
    libc++abi-dev	\
    libssl-dev	        \
    lld		        \
    lldb		\
    man		        \
    neovim		\
    ninja		\
    pkg-config          \
    python3		\
    python3-pip         \
    python3-venv        \
    tmux		\
    unzip		\
    wget

# Download first round of python tools
pip3 install -U pip
python3 -m pip install pipx

# Download and install rust
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

# Gets the config files for bash, neovim, and tmux
git clone https://gitlab.com/sweenish/dotfiles ~/.dotfiles

# Start linking the settings files just downloaded
if [[ -f ~/.bashrc ]]; then
    echo "Backing up .bashrc"
    mv ~/.bashrc ~/.bashrc.BAK
fi

# Ensure we are in our home directory & symlink new bash settings
cd && ln -s ~/.dotfiles/.bashrc ~/.bashrc

if [[ -f ~/.bash_aliases ]]; then
    echo "Backing up .bash_aliases"
    mv ~/.bash_aliases ~/.bash_aliases.BAK
fi

# Ensure we are in our home directory & symlink new aliases
cd && ln -s ~/.dotfiles/.bash_aliases ~/.bash_aliases

if [[ -d ~/.config/nvim ]]; then
    echo "Backing up existing nvim config"
    mv ~/.config/nvim ~/.config/nvim.BAK
fi

# Ensure we are in our home directory & symlink nvim config
cd && ln -s ~/.dotfiles/.config/nvim ~/.config/nvim
