# Environment
set -Ux ANDROID_HOME /Users/sweenish/Library/Android/sdk
set -Ux CARGO_HOME /Users/sweenish/\x2ecargo
set -Ux CMAKE_GENERATOR Ninja
set -Ux COPYFILE_DISABLE 1
set -Ux RUSTUP_HOME /Users/sweenish/\x2erustup

# Aliases
alias ...='cd ../..'
alias ....='cd ../../..'
alias ls='ls --color'
alias ll='ls -lh'
alias lll='ls -alh'

# Abbreviations
abbr --ad tar='tar --no-mac-metadata --no-xattrs'

# Path
set PATH $PATH /Users/sweenish/Library/Python/3.10/bin ~/.local/bin ~/.cargo/bin

# Get prompt, starship is a separate install
starship init fish | source
