function resetLaunchPad
    read -l -P 'Do you really want to reset LaunchPad (y/N): ' choice
    
    if string match --regex '^[Yy]' $choice;
        defaults write com.apple.dock ResetLaunchPad -bool true
        killall Dock
    end 
end
