return require('packer').startup(function()
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'

  -- nvim-tree
  use {
    'kyazdani42/nvim-tree.lua',
    requires = 'kyazdani42/nvim-web-devicons',
    config = function() require'nvim-tree'.setup {} end
  }

  -- airline
  use {
    'vim-airline/vim-airline',
    requires = 'vim-airline/vim-airline-themes'
  }

  -- Rigel Theme
  use 'Rigellute/rigel'

  -- Treesitter
  use {
    'nvim-treesitter/nvim-treesitter',
    run = ':TSUpdate'
  }

  -- Telescope FZF Native
  use {
      'nvim-telescope/telescope-fzf-native.nvim',
      run = 'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build'
  }

  -- Telescope
  use {
    'nvim-telescope/telescope.nvim',
    requires = { {'nvim-lua/plenary.nvim'} }
  }

  -- Auto pairs
  use 'LunarWatcher/auto-pairs'

  -- vim surround
  use 'tpope/vim-surround'

  -- tmuxline
  use 'edkolev/tmuxline.vim'

  --
  -- nvim-cmp (auto-completion)
  --
  use 'hrsh7th/nvim-lspconfig'
  use 'hrsh7th/nvim-cmp'
  use 'hrsh7th/cmp-nvim-lsp'
  use 'saadparwaiz1/cmp_luasnip'
  use 'L3MON4D3/LuaSnip'

  end)
