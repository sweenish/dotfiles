-- General Settings
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.wrap = false
vim.opt.tabstop = 8
vim.opt.expandtab = true
vim.opt.shiftwidth = 4
vim.opt.splitbelow = true
vim.opt.splitright = true

-- Packer
require('plugins')

-- Spellcheck for certain filetypes
vim.api.nvim_create_autocmd("Filetype", {
    pattern = { "markdown" },
    command = [[ setlocal spell ]]}
)

-- Hide default bottom bar in favor of airline plugin
vim.opt.showmode = false

-- Color Scheme
vim.opt.termguicolors = true
vim.opt.background = "dark"
vim.cmd [[ colorscheme Rigel ]]

-- Airline Theme
vim.g.airline_powerline_fonts = 1
vim.g.rigel_airline = 1
vim.g.airline_theme = "rigel"

-- Highlight Trailing Whitespace
vim.cmd [[
    augroup NoTrailingWhitespace
        highlight ExtraWhitespace ctermbg=red guibg=red
        match ExtraWhitespace /\s\+$/
        autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
        autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
        autocmd InsertLeave * match ExtraWhitespace /\s\+$/
        autocmd BufWinLeave * call clearmatches()
    augroup end
]]

-- Highlight characters on long lines
vim.cmd [[
    augroup NoLongLines
        highlight ColorColumn ctermbg=magenta guibg=magenta
        call matchadd('ColorColumn', '\%101v.\+', 100)
    augroup end
]]

-- nvim-tree setup
require("nvim-tree").setup({
    respect_buf_cwd = true
})
vim.api.nvim_buf_set_keymap(0, 'n', '<C-n>', '<cmd>NvimTreeToggle<CR>', { noremap = true })

local custom_lsp_attach = function()
    vim.api.nvim_buf_set_keymap(0, 'n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', {noremap = true})
    vim.api.nvim_buf_set_keymap(0, 'n', '<C-]>', '<cmd>lua vim.lsp.buf.definition()<CR>', {noremap = true})

    -- i_C-x_C-o
    vim.api.nvim_buf_set_option(0, 'omnifunc', 'v:lua.vim.lsp.omnifunc')
    end

-- Add additional capabilities supported by nvim-cmp
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

-- Enable some language servers with the additioanl completion capabilites offered by nvim-cmp
local servers = { 'clangd', 'pyright' }
for _, lsp in ipairs(servers) do
    require('lspconfig')[lsp].setup {
        on_attach = custom_lsp_attach,
        capabilities = capabilities,
    }
end

-- Set completeopt to have a better completion experience
vim.o.completeopt = 'menuone,noselect'

-- Luasnip setup
local luasnip = require 'luasnip'

-- nvim-cmp setup
local cmp = require 'cmp'
cmp.setup {
    snippet = {
        expand = function(args)
            require('luasnip').lsp_expand(args.body)
        end,
    },
    mapping = {
        ['<C-p>'] = cmp.mapping.select_prev_item(),
        ['<C-n>'] = cmp.mapping.select_next_item(),
        ['<C-d>'] = cmp.mapping.scroll_docs(-4),
        ['<C-f>'] = cmp.mapping.scroll_docs(4),
        ['<C-Space>'] = cmp.mapping.complete(),
        ['<C-e>'] = cmp.mapping.close(),
        ['<CR>'] = cmp.mapping.confirm {
            behavior = cmp.ConfirmBehavior.Replace,
            select = true,
        },
        ['<Tab>'] = function(fallback)
            if cmp.visible() then
                cmp.select_next_item()
            elseif luasnip.expand_or_jumpable() then
                luasnip.expand_or_jump()
            else
                fallback()
            end
        end,
        ['<S-Tab>'] = function(fallback)
            if cmp.visible() then
                cmp.select_prev_item()
            elseif luasnip.jumpable(-1) then
                luasnip.jump(-1)
            else
                fallback()
            end
        end,
    },
    sources = {
        { name = 'nvim_lsp' },
        { name = 'luasnip' },
    },
}

require'nvim-treesitter.configs'.setup {
  actions = {
    open_file = {
      quit_on_open = true,
    },
  },
  highlight = {
    enable = true,
    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
    -- Using this option may slow down your editor, and you may see some duplicate highlights.
    -- Instead of true it can also be a list of languages
    additional_vim_regex_highlighting = false,
    respect_buf_cwd = 1,
  },
}
