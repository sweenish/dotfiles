if [ -d /usr/local/bin/brew ]; then
  eval "$(/usr/local/bin/brew shellenv)"
fi

# Completions
zstyle ':completion:*' completer _expand _complete _ignored _approximate
zstyle ':completion:*' max-errors 2 numeric
zstyle :compinstall filename "$HOME/.zshrc"

# History
HISTFILE="$HOME/.histfile"
HISTSIZE=50000
SAVEHIST=10000

# No duplicates in history
setopt EXTENDED_HISTORY
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_FIND_NO_DUPS
setopt HIST_SAVE_NO_DUPS
setopt HIST_BEEP

# Source extra shell stuff
if [ -d $HOME/.dotfiles/.shrc.d ]; then
  for f in $HOME/.dotfiles/.shrc.d/* ; do
    . "$f"
  done
fi

if [ -f $HOME/.dotfiles/plugins/zsh/zsh-abbr/zsh-abbr.zsh ]; then
  . $HOME/.dotfiles/plugins/zsh/zsh-abbr/zsh-abbr.zsh
else
  echo "zsh-abbr not found. Submodules likely not installed"
fi

# pyenv
export PYENV_ROOT="$HOME/.pyenv"
[[ -d $PYENV_ROOT/bin ]] && export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"

# Finalizing PATH
export PATH="$HOME/.local/bin:$HOME/go/bin:$PATH"

# Colors!
export COLORTERM=truecolor

# Start ssh agent and add relevant keys
ssh-add $HOME/.ssh/gitlab &> /dev/null 
ssh-add $HOME/.ssh/github &> /dev/null 

# OS-specific stuff
## Mac
if [[ "$OSTYPE" == "darwin"*  ]]; then
  # If brew LLVM is installed, we want to use it
  if [ -d "/usr/local/opt/llvm" ]; then
    export LDFLAGS="-L/usr/local/opt/llvm/lib/c++ -Wl,-rpath,/usr/local/opt/llvm/lib/c++"
    export CPPFLAGS="-I/usr/local/opt/llvm/include"
    export PATH="/usr/local/opt/llvm/bin:$PATH"
  fi
fi

# Pure prompt
fpath+=($HOME/.dotfiles/plugins/zsh/pure)
autoload -U promptinit; promptinit
prompt pure

# Poetry completions
fpath+=~/.zfunc

# bun completions
[ -s "/Users/sweenish/.bun/_bun" ] && source "/Users/sweenish/.bun/_bun"

# bun
export BUN_INSTALL="$HOME/.bun"
export PATH="$BUN_INSTALL/bin:$PATH"

autoload -Uz compinit
compinit

